# Astrophysics Data Retrieval Scripts



## Description

A collection of scripts and tools to retrieve astrophysical data to be visualized in [VIRUP](http://gitlab.com/Dexter9313/VIRUP).
